let drawFlag: boolean = false;							// マウス＆タッチパネル操作用変数 - 押下フラグ
let prev: any = null;								// マウス＆タッチパネル操作用変数 - 前回位置
let cPushArray: any = new Array();					// undo&redo機能用変数 - 画像配列
let cStep: any = -1;									// undo&redo機能用変数 - 画像配列の現在index


// ページの読み込み完了時に実行
window.addEventListener("load", function () {
    let can = <HTMLCanvasElement>document.getElementById("myCanvas");	// お絵かき用canvas
    let paintForKids = new PaintForKids(can);
    paintForKids.initialize();
}, true);


class PaintForKids {
    constructor(
        public readonly canvas: HTMLCanvasElement, 
    ) {}

    initialize(){
        let can = this.canvas;
        let ctx = <CanvasRenderingContext2D>can.getContext("2d");					// お絵かき用canvasの context
		
        // 初期設定 - canvas
		can.height = (window.innerHeight - can.getBoundingClientRect().top) * 0.9;
		can.width = (window.innerWidth - can.getBoundingClientRect().left) * 0.9;
		ctx.strokeStyle = "rgba(255,255,255,1)";
		ctx.fillStyle = "rgba(255,255,255,1)";
		ctx.fillRect(0, 0, can.width, can.height);
        pushCanvas();


		// // 初期設定 - 塗り絵
		// let nurieImg = new Image();
		// nurieImg.src = nuries[1].path;
		// nurieImg.addEventListener("load", function () {
		// 	let tempHeight = can.height;
		// 	let tempWidth = can.width;

		// 	if (nurieImg.height * can.width / nurieImg.width <= can.height) {
		// 		tempHeight = nurieImg.height * can.width / nurieImg.width;
		// 	} else {
		// 		tempWidth = nurieImg.width * can.height / nurieImg.height;
		// 	};

		// 	ctx.drawImage(nurieImg, 0, 0, tempWidth, tempHeight);
		// }, false);


		// マウス操作用
		can.addEventListener("mousedown", mouseStart, false);
		can.addEventListener("mousemove", mouseDraw, true);
		can.addEventListener("mouseup", end, false);

		// タッチパネル操作用
		can.addEventListener("touchstart", start, false);
		can.addEventListener("touchmove", draw, true);
		can.addEventListener("touchend", end, false);

		// ボタン操作用
        let undoCanvasButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("undoCanvas");
        undoCanvasButton.addEventListener("click", undoCanvas);
        let redoCanvasButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("redoCanvas");
        redoCanvasButton.addEventListener("click", redoCanvas);
        let saveCanvasButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("saveCanvas");
        saveCanvasButton.addEventListener("click", saveCanvas);

		// 初期設定 - 線の色選択
		let penColsElm: HTMLElement = <HTMLElement>document.getElementById("penCols")
		penColsElm.innerHTML = ""

		for (let i = 0; i < penCols.length; i++) {
            let penColElm: HTMLSpanElement = document.createElement('span');
            penColElm.id = penCols[i].id;

            penColElm.innerHTML = penCols[i].name;
			penColElm.style.background = penCols[i].bgCode;
			penColElm.style.color = penCols[i].textCode;
            penColElm.addEventListener("click",  () => changePenColById(penColElm.id));

            penColsElm.append(penColElm);
		}

		changePenCol(0);
    }
}

function changePenColById (id: string) : void {
    let idx: number = selectPenColIdxById(id)
    changePenCol(idx);
}

function selectPenColIdxById (id: string) : number {
    for (let i = 0; i < penCols.length; i++) {
        let penCol = penCols[i];
        if (penCol.id === id) { return i };
    }
    throw new Error("Invalid id: " + id)
}

// 色クラスを定義
export class PenCol {
    constructor(
        public readonly id: string,
        public readonly name: string,
        public readonly rbga: string,
        public readonly bgCode: string,
        public readonly textCode: string,
    ) {}
}

// 色インスタンスを設定
var penCols: PenCol[] = [];
penCols[0] = new PenCol("black", "くろ", "rgba(0,0,0,1)", "#000000", "#FFFFFF");
penCols[1] = new PenCol("white", "しろ", "rgba(255,255,255,1)", "#FFFFFF", "#000000");
penCols[2] = new PenCol("red", "あか", "rgba(255,0,0,1)", "#FF0000", "#FFFFFF");
penCols[3] = new PenCol("green", "みどり", "rgba(0,255,0,1)", "#00FF00", "#000000");
penCols[4] = new PenCol("blue", "あお", "rgba(0,0,255,1)", "#0000FF", "#FFFFFF");
penCols[5] = new PenCol("yellow", "きいろ", "rgba(255,255,0,1)", "#FFFF00", "#000000");
penCols[6] = new PenCol("aqua", "みずいろ", "rgba(0,255,255,1)", "#00FFFF", "#000000");
penCols[7] = new PenCol("purple", "むらさき", "rgba(255,0,255,1)", "#FF00FF", "#000000");


// 塗り絵クラスを定義
class Nurie {
    constructor(
        public readonly id: string,
        public readonly name: string,
        public readonly path: string,
    ) {}
}

// 塗り絵インスタンスを設定
var nuries: Nurie[] = [];
nuries[0] = new Nurie("Null", "(なし)", "_blank");
nuries[1] = new Nurie("Tdr", "Tdr", "./img-nurie/Tdr.png");


// マウス操作用処理 - マウス押下
function mouseStart(e: MouseEvent) {
    e.preventDefault();
    prev = { X: e.clientX, Y: e.clientY };
    drawFlag = true;
}

// マウス操作用処理 - マウス移動
function mouseDraw(e: MouseEvent) {
    e.preventDefault();
    if (!drawFlag) return;
    var pos = { X: e.clientX, Y: e.clientY };
    drawLine(prev.X, prev.Y, pos.X, pos.Y);
    prev = pos;
}

// タッチパネル操作用処理 - タッチ開始
function start(e: TouchEvent) {
    e.preventDefault();
    var touch = e.touches[0];
    prev = { X: touch.pageX, Y: touch.pageY };
    drawFlag = true;
}

// タッチパネル操作用処理 - タッチ移動
function draw(e: TouchEvent) {
    e.preventDefault();
    if (!drawFlag) return;
    var touch = e.touches[0];
    var pos = { X: touch.pageX, Y: touch.pageY };
    drawLine(prev.X, prev.Y, pos.X, pos.Y);
    prev = pos;
}

// マウス＆タッチパネル操作用処理 - マウス・タッチ終了
function end(e: MouseEvent | TouchEvent) {
    e.preventDefault()
    prev = null;
    drawFlag = false;
    pushCanvas();
}

// マウス＆タッチパネル操作用処理 - 線を描く
function drawLine(prevX: number, prevY: number, posX: number, posY: number) {
    let can = <HTMLCanvasElement>document.getElementById("myCanvas");	// お絵かき用canvas
    let ctx = <CanvasRenderingContext2D>can.getContext("2d");					// お絵かき用canvasの context
    // 線の位置補正用に、キャンパスの位置を取得
    var canvasRect = can.getBoundingClientRect();

    var canOrg = { X: canvasRect.left, Y: canvasRect.top };

    ctx.lineWidth = 5;
    ctx.beginPath();
    ctx.moveTo(prevX - canOrg.X, prevY - canOrg.Y);
    ctx.lineTo(posX - canOrg.X, posY - canOrg.Y);
    ctx.stroke();
}

// 線の色変更処理
export function changePenCol(idx: number) {
    let can = <HTMLCanvasElement>document.getElementById("myCanvas");	// お絵かき用canvas
    let ctx = <CanvasRenderingContext2D>can.getContext("2d");					// お絵かき用canvasの context
    ctx.strokeStyle = penCols[idx].rbga;

    let penColNow = <HTMLElement>document.getElementById("penColor");
    penColNow.innerHTML = penCols[idx].name;
    penColNow.style.color = penCols[idx].textCode;
    penColNow.style.background = penCols[idx].bgCode;
}

// 画像保存処理
export function saveCanvas() {
    let can = <HTMLCanvasElement>document.getElementById("myCanvas");	// お絵かき用canvas
    let strDataURI = can.toDataURL("image/png");
    let winSave: WindowProxy = <WindowProxy>window.open("", "_blank");
    // winSave.document.write("<p>画像を右クリック または 長押しして保存してください</p><img src=\"" + strDataURI + "\" border=1/>");
    winSave.document.write("<img alt='yourPicture' src='" + strDataURI + "' border='1' />");
}

// 画像配列更新処理
function pushCanvas() {
    let can = <HTMLCanvasElement>document.getElementById("myCanvas");	// お絵かき用canvas
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; };
    cPushArray.push(can.toDataURL("image/png"));
}

// 画像戻し処理
export function undoCanvas() {
    let can = <HTMLCanvasElement>document.getElementById("myCanvas");	// お絵かき用canvas
    let ctx = <CanvasRenderingContext2D>can.getContext("2d");					// お絵かき用canvasの context
    if (0 < cStep) {
        cStep--;
        var canPic = new Image();
        canPic.src = cPushArray[cStep];
        canPic.addEventListener("load", function () {
            ctx.drawImage(canPic, 0, 0);
        }, false);
    }
}

// 画像やり直し処理
export function redoCanvas() {
    let can = <HTMLCanvasElement>document.getElementById("myCanvas");	// お絵かき用canvas
    let ctx = <CanvasRenderingContext2D>can.getContext("2d");					// お絵かき用canvasの context
    if (cStep < cPushArray.length - 1) {
        cStep++;
        var canPic = new Image();
        canPic.src = cPushArray[cStep];
        canPic.addEventListener("load", function () {
            ctx.drawImage(canPic, 0, 0);
        }, false);
    }
}
